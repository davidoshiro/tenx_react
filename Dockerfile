FROM ruby:2.6.3

ARG APP_NAME

RUN apt-get update -qq \
  && apt-get install -y apt-utils \
  && apt-get install -y build-essential libpq-dev imagemagick libnss3 \
  && mkdir -p /usr/share/www/$APP_NAME \
  && mkdir -p /usr/share/node_modules

# nodejs
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
  && apt-get install -y nodejs

# yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && apt-get install -y apt-transport-https ca-certificates \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update -qq \
  && apt-get install -y yarn \
  && apt-get clean \
  && echo "--install.modules-folder /usr/share/node_modules" > /.yarnrc

WORKDIR /usr/share/www/$APP_NAME

COPY Gemfile /usr/share/www/$APP_NAME/Gemfile
COPY Gemfile.lock /usr/share/www/$APP_NAME/Gemfile.lock

RUN bundle install \
  && npm install

COPY . ./
